import request from '../../utils/request';
var qs = require('qs');

//  获取用户管理数据
export function getUserService(obj) {
  let data = {
    "cmd": "findUserListAll",
    "token": "123123",
    "pageNum": obj.pageNum,
    "pageSize": obj.pageSize,
    "name": obj.name,
    "depa": obj.depa,
    "section": obj.section,
    "roleName": obj.roleName,
    "userName": obj.userName
  }; 
  return request({
    url: 'http://10.4.249.119:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}
// 用户账户 增(1),删(3),改(2)
export function setUserOperateService(obj) {
  let data = {
    "cmd":"userOpt",
    "token":"123123",
    "userId": obj.userId,
    "name": obj.name,
    "password": obj.password,
    "userName": obj.userName,
    "mobile": obj.mobile,
    "email": obj.email,
    "remark": obj.remark,
    "type": obj.type
  };
  return request({
    url: 'http://10.4.249.103:8087/api',
    method: 'post',
    data: {
      sendmsg: obj
    }
  });
}

// 批量删除
export function setDeleteLotService(obj) {
  let data = {
    "cmd": "removeUsers",
    "token": "123123",
    "ids": obj
  }; 
  return request({
    url: 'http://10.4.249.103:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}
