import request from '../../utils/request';
var qs = require('qs');

//  获取权限基础数据列表
export function permissionService(pageNum, pageSize) {
  let data = {
    "cmd": "findRightList",
    "token": "1122",
    "pageNum": pageNum,
    "pageSize": pageSize
  }; 
  return request({
    url: 'http://10.4.249.119:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}
