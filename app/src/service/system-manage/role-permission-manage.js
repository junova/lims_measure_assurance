import request from '../../utils/request';
var qs = require('qs');

//  获取角色与权限管理列表(分页、查询)
export function getRolePermissionService(pageNum, pageSize, roleName) {
  let data = {
    "cmd": "findRoleList",
    "token": "123123",
    "pageNum": pageNum,
    "pageSize": pageSize,
    "roleName": roleName
  };
  return request({
    url: 'http://10.4.249.119:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

// 设置拥有人
export function setOwnerService(rowid, pageSize, pageNum) {
  let data = {
    "cmd": "findUserListByRoleId",
    "token":"1122",
    "roleId": rowid,
    "pageSize": pageSize,
    "pageNum": pageNum
  };
  return request({
    url: 'http://10.4.249.119:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

// 设置权限
export function setPermissionService() {
  let data = {
    "cmd":"findTree",
    "token":"1122"
  };
  return request({
    url: 'http://10.4.249.119:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

// 新增（1）、修改（2）、删除（3）角色
export function setRoleOperateService(roleId, roleName, type) {
  let data = {
    "cmd": "roleOpt",
    "token": "123123",
    "roleId": roleId,
    "roleName": roleName,
    "type": type
  }; 
  return request({
    url: 'http://10.4.249.103:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

// 批量删除
export function setDeleteLotService(selectVal) {
  let data = {
    "cmd": "removeRoles",
    "token": "123123",
    "ids": selectVal
  };
  return request({
    url: 'http://10.4.249.103:8087/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

