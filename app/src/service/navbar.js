// 获取导航栏列表
export function getNavbarListService() {
  var list = [{
      index: 1,
      name: '检测管理',
      sub: [{
          name: '新计量器具验收',
          hasSub: false,
          path: '/main/test-manage/tool-check-accept'
        },
        {
          name: '计量器具状态变更',
          hasSub: false,
          path: '/main/test-manage/tool-status-change'
        },
        {
          name: '周检计划管理',
          hasSub: false,
          path: '/main/test-manage/week-test-plan'
        }
      ]
    },
    {
      index: 2,
      name: '任务单管理',
      sub: [{
          name: '业务负责人审核',
          hasSub: false,
          path: '/main/job-order-manage/check-by-bussinessowner'
        },
        {
          name: '计量人员审核',
          hasSub: false,
          path: '/main/job-order-manage/check-by-metrologyperson'
        },
        {
          name: '下属企业领导审核',
          hasSub: false,
          path: '/main/job-order-manage/check-by-leader'
        },
        {
          name: '任务单指派',
          hasSub: false,
          path: '/main/job-order-manage/job-order-assign'
        },
        {
          name: '任务单历史记录',
          hasSub: false,
          path: '/main/job-order-manage/job-order-history'
        }
      ]
    },
    {
      index: 3,
      name: '台账管理',
      sub: [{
          name: '基础信息管理',
          hasSub: false,
          path: '/main/standing-book-manage/basic-info-manage'
        },
        {
          name: '对照表维护',
          hasSub: false,
          path: '/main/standing-book-manage/contrast-table-maintain'
        },
        {
          name: '基础信息报废库',
          hasSub: false,
          path: '/main/standing-book-manage/basic-info-scrap'
        }
      ]
    },
    {
      index: 4,
      name: '待检器具管理',
      sub: [{
          name: '新报验器具送检',
          hasSub: false,
          path: '/main/wait-test-tool-manage/apply-test-inspect'
        },
        {
          name: '新报验器具归还',
          hasSub: false,
          path: '/main/wait-test-tool-manage/apply-test-return'
        },
        {
          name: '内部周检器具送检',
          hasSub: false,
          path: '/main/wait-test-tool-manage/inside-check-inspect'
        },
        {
          name: '内部周检器具归还',
          hasSub: false,
          path: '/main/wait-test-tool-manage/inside-check-return'
        },
        {
          name: '报废处理',
          hasSub: false,
          path: '/main/wait-test-tool-manage/scrap-dealwith'
        }
      ]
    },
    {
      index: 5,
      name: '实验室报告管理',
      sub: [{
          name: '新报验实验报告',
          hasSub: false,
          path: '/main/lab-report-manage/inspect-lab-report'
        },
        {
          name: '检定管理',
          hasSub: false,
          path: '/main/lab-report-manage/verification-manage'
        },
        {
          name: '偏差管理',
          hasSub: false,
          path: '/main/lab-report-manage/deviation-manage'
        },
        {
          name: 'MSA报告管理',
          hasSub: false,
          path: '/main/lab-report-manage/msa-report-manage'
        },
        {
          name: '报告归档',
          hasSub: false,
          path: '/main/lab-report-manage/archive-report'
        },
        {
          name: '新报验器具合格库',
          hasSub: false,
          path: '/main/lab-report-manage/inspect-tool-standard'
        },
        {
          name: '新报验实验报告更新',
          hasSub: false,
          path: '/main/lab-report-manage/inspect-report-refresh'
        },
        {
          name: '新报验实验报告归档',
          hasSub: false,
          path: '/main/lab-report-manage/inspect-report-archive'
        }
      ]
    },
    {
      index: 6,
      name: '系统管理',
      sub: [{
          name: '周检管理',
          hasSub: false,
          path: '/main/system-manage/week-check-manage'
        },
        {
          name: '用户管理',
          hasSub: false,
          path: '/main/system-manage/user-manage'
        },
        {
          name: '角色和权限管理',
          hasSub: false,
          path: '/main/system-manage/role-permission-manage'
        },
        {
          name: '权限基础数据',
          hasSub: false,
          path: '/main/system-manage/permission-data'
        }
      ]
    },
    {
      index: 7,
      name: '帮助中心',
      sub: [{
          name: '使用说明',
          hasSub: false
        }
      ]
    }
  ];
  return list;
}
