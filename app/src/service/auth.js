import request from '../utils/request';
var qs = require('qs');

// 登陆
// export function loginService(obj) {
//   console.log(obj);
//   const data = obj;
//   return request({
//     url: 'userLogin/login_login',
//     method: 'post',
//     data,
//   });
// }
export function loginService(name, password) {
  let data = {
    "cmd": "getToken",
    "username": name,
    "password": password
  };
  return request({
    url: 'http://10.1.10.202:8083/api',
    method: 'post',
    data: {
      sendmsg: data
    }
  });
}

// 注销
export function logoutService(username) {
  const data = {
    'userName': username,
  };
  return request({
    url: 'userLogin/logout',
    method: 'post',
    data: qs.stringify(data),
  })
}
