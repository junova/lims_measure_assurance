//系统管理模块路由
import WeekCheckManage from '../../view/main/system-manage/week-check-manage';
import UserManage from '../../view/main/system-manage/user-manage';
import RolePermissionManage from '../../view/main/system-manage/role-permission-manage';
import PermissionData from '../../view/main/system-manage/permission-data';

export default [{
  path: '/main/system-manage/week-check-manage',
  name: 'WeekCheckManage',
  component: WeekCheckManage,
  meta: {
    title: '周检管理'
  }
},{
  path: '/main/system-manage/user-manage',
  name: 'UserManage',
  component: UserManage,
  meta: {
    title: '用户管理'
  }
},{
  path: '/main/system-manage/role-permission-manage',
  name: 'RolePermissionManage',
  component: RolePermissionManage,
  meta: {
    title: '角色和权限管理'
  }
},{
  path: '/main/system-manage/permission-data',
  name: 'PermissionData',
  component: PermissionData,
  meta: {
    title: '权限基础数据'
  }
}]
