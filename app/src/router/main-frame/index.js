import TestTableFilter from '../../view/test-table-filter';
import Abstract from '../../view/main/abstract';
import Backlog from '../../view/main/index/backlog';
import TaskStatistical from '../../view/main/index/task-statistical';
import TaskQuery from '../../view/main/index/task-query';
import SummarySheet from '../../view/main/index/summary-sheet';
import TestManage from './test-manage';
import JobOrderManage from './job-order-manage';
import StandingBookManage from './standing-book-manage';
import WaitTestToolManage from './wait-test-tool-manage';
import SystemManage from './system-manage';
import LabReportManage from './lab-report-manage';

export default [{
    path: '/main/test',
    name: 'test-table-filter',
    component: TestTableFilter,
  }, {
    path: '/main/abstract',
    name: 'abstract',
    component: Abstract,
    meta: {
      title: '首页'
    }
  }, {
    path: '/main/backlog',
    name: 'Backlog',
    component: Backlog,
    meta: {
      title: '我的待办'
    }
  }, {
    path: '/main/task-statistical',
    name: 'TaskStatistical',
    component: TaskStatistical,
    meta: {
      title: '任务统计'
    }
  }, {
    path: '/main/task-query',
    name: 'TaskQuery',
    component: TaskQuery,
    meta: {
      title: '任务查询'
    }
  }, {
    path: '/main/summary-sheet',
    name: 'SummarySheet',
    component: SummarySheet,
    meta: {
      title: '量检具报验汇总统计表'
    }
  },
  ...TestManage,
  ...JobOrderManage,
  ...StandingBookManage,
  ...WaitTestToolManage,
  ...SystemManage,
  ...LabReportManage
];
