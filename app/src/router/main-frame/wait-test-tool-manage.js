//待见器具管理模块路由
import ApplyTestInspect from '../../view/main/wait-test-tool-manage/apply-test-inspect';
import ApplyTestReturn from '../../view/main/wait-test-tool-manage/apply-test-return';
import InsideCheckInspect from '../../view/main/wait-test-tool-manage/inside-check-inspect';
import InsideCheckReturn from '../../view/main/wait-test-tool-manage/inside-check-return';
import ScrapDealwith from '../../view/main/wait-test-tool-manage/scrap-dealwith';

export default [{
  path: '/main/wait-test-tool-manage/apply-test-inspect',
  name: 'ApplyTestInspect',
  component: ApplyTestInspect,
  meta: {
    title: '新报验器具送检'
  }
},{
  path: '/main/wait-test-tool-manage/apply-test-return',
  name: 'ApplyTestReturn',
  component: ApplyTestReturn,
  meta: {
    title: '新报验器具归还'
  }
},{
  path: '/main/wait-test-tool-manage/inside-check-inspect',
  name: 'InsideCheckInspect',
  component: InsideCheckInspect,
  meta: {
    title: '内部周检器具送检'
  }
},{
  path: '/main/wait-test-tool-manage/inside-check-return',
  name: 'InsideCheckReturn',
  component: InsideCheckReturn,
  meta: {
    title: '内部周检器具归还'
  }
},{
  path: '/main/wait-test-tool-manage/scrap-dealwith',
  name: 'ScrapDealwith',
  component: ScrapDealwith,
  meta: {
    title: '报废处理'
  }
}]
