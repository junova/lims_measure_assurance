//实验报告管理模块路由
import InspectLabReport from '../../view/main/lab-report-manage/inspect-lab-report';
import VerificationManage from '../../view/main/lab-report-manage/verification-manage';
import DeviationManage from '../../view/main/lab-report-manage/deviation-manage';
import MSAReportManage from '../../view/main/lab-report-manage/msa-report-manage';
import ArchiveReport from '../../view/main/lab-report-manage/archive-report';
import InspectToolStandard from '../../view/main/lab-report-manage/inspect-tool-standard';
import InspectReportRefresh from '../../view/main/lab-report-manage/inspect-report-refresh';
import InspectReportArchive from '../../view/main/lab-report-manage/inspect-report-archive';

export default [{
  path: '/main/lab-report-manage/inspect-lab-report',
  name: 'InspectLabReport',
  component: InspectLabReport,
  meta: {
    title: '新报验实验报告'
  }
},{
  path: '/main/lab-report-manage/verification-manage',
  name: 'VerificationManage',
  component: VerificationManage,
  meta: {
    title: '检定管理'
  }
},{
  path: '/main/lab-report-manage/deviation-manage',
  name: 'DeviationManage',
  component: DeviationManage,
  meta: {
    title: '偏差管理'
  }
},{
  path: '/main/lab-report-manage/msa-report-manage',
  name: 'MSAReportManage',
  component: MSAReportManage,
  meta: {
    title: 'MSA报告管理'
  }
},{
  path: '/main/lab-report-manage/archive-report',
  name: 'ArchiveReport',
  component: ArchiveReport,
  meta: {
    title: '报告归档'
  }
},{
  path: '/main/lab-report-manage/inspect-tool-standard',
  name: 'InspectToolStandard',
  component: InspectToolStandard,
  meta: {
    title: '新报验器具合格库'
  }
},{
  path: '/main/lab-report-manage/inspect-report-refresh',
  name: 'InspectReportRefresh',
  component: InspectReportRefresh,
  meta: {
    title: '新报验实验报告更新'
  }
},{
  path: '/main/lab-report-manage/inspect-report-archive',
  name: 'InspectReportArchive',
  component: InspectReportArchive,
  meta: {
    title: '新报验实验报告归档'
  }
}]
