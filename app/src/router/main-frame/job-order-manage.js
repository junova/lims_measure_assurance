//任务单模块路由
import CheckByBussinessowner from '../../view/main/job-order-manage/check-by-bussinessowner';
import CheckByMetrologyperson from '../../view/main/job-order-manage/check-by-metrologyperson';
import CheckByLeader from '../../view/main/job-order-manage/check-by-leader';
import JobOrderAssign from '../../view/main/job-order-manage/job-order-assign';
import JobOrderHistory from '../../view/main/job-order-manage/job-order-history';

export default [{
  path: '/main/job-order-manage/check-by-bussinessowner',
  name: 'CheckByBussinessowner',
  component: CheckByBussinessowner,
  meta: {
    title: '业务负责人审核'
  }
}, {
  path: '/main/job-order-manage/check-by-metrologyperson',
  name: 'CheckByMetrologyperson',
  component: CheckByMetrologyperson,
  meta: {
    title: '计量人员审核'
  }
}, {
  path: '/main/job-order-manage/check-by-leader',
  name: 'CheckByLeader',
  component: CheckByLeader,
  meta: {
    title: '下属企业领导审核'
  }
}, {
  path: '/main/job-order-manage/job-order-assign',
  name: 'JobOrderAssign',
  component: JobOrderAssign,
  meta: {
    title: '任务单指派'
  }
}, {
  path: '/main/job-order-manage/job-order-history',
  name: 'JobOrderHistory',
  component: JobOrderHistory,
  meta: {
    title: '任务单历史记录'
  }
}]

