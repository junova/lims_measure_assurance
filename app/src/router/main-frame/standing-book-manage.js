//台账管理模块路由
import BasicInfoManage from '../../view/main/standing-book-manage/basic-info-manage';
import ConstrastTableMaintain from '../../view/main/standing-book-manage/contrast-table-maintain';
import BasicInfoScrap from '../../view/main/standing-book-manage/basic-info-scrap';

export default [{
  path: '/main/standing-book-manage/basic-info-manage',
  name: 'BasicInfoManage',
  component: BasicInfoManage,
  meta: {
    title: '基础信息管理'
  }
},{
  path: '/main/standing-book-manage/contrast-table-maintain',
  name: 'ConstrastTableMaintain',
  component: ConstrastTableMaintain,
  meta: {
    title: '对照表维护'
  }
},{
  path: '/main/standing-book-manage/basic-info-scrap',
  name: 'BasicInfoScrap',
  component: BasicInfoScrap,
  meta: {
    title: '基础信息报废库'
  }
}]
