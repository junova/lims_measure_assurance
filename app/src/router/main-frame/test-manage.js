//检测管理模块路由
import ToolCheckAccept from '../../view/main/test-manage/tool-check-accept';
import ToolStatusChange from '../../view/main/test-manage/tool-status-change';
import WeekTestPlan from '../../view/main/test-manage/week-test-plan';

export default [{
  path: '/main/test-manage/tool-check-accept',
  name: 'ToolCheckAccept',
  component: ToolCheckAccept,
  meta: {
    title: '新计量器具验收'
  }
},{
  path: '/main/test-manage/tool-status-change',
  name: 'ToolStatusChange',
  component: ToolStatusChange,
  meta: {
    title: '计量器具状态变更'
  }
},{
  path: '/main/test-manage/week-test-plan',
  name: 'WeekTestPlan',
  component: WeekTestPlan,
  meta: {
    title: '周检计划管理'
  }
}]
