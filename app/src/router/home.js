import Index from '../view/home/index';
import Login from '../view/home/login';

export default [{
  path: '/home/index',
  name: 'index',
  component: Index,
}, {
  path: '/',
  name: 'login',
  component: Login,
  meta: {
    withoutLogin: true,
  }
}];
