import Vue from 'vue';
import Router from 'vue-router';
import MainFrame from '../view/main-frame.vue';

import Index from './main-frame/index';
import Home from './home';
import JtBread from 'components/jt-breadcrumb';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
      path: '/main',
      component: MainFrame,
      children: [
        ...Index,
      ]
    },
    ...Home,
    {
      path:'/test',
      component: JtBread,
      name:'test',
      meta:{
        title:'测试'
      }
    }
  ]
});

// 拦截路由
// 对路由进行鉴权
// router.beforeEach((to, from, next) => {
//   // 判断是否需要登陆权限
//   if (to.matched.some(res => res.meta.withoutLogin)) {
//     next();
//   } else {
//     // 判断是否登陆
//     if (localStorage.getItem("token")) {
//       // TODO:检查登陆
//       next();
//     } else {
//       next({
//         path: '/'
//       });
//     }
//   }
// });

// 空路由判断
router.beforeEach((to,form,next)=>{
  if(to.matched.length===0){
    next({
      path:'/main/abstract',
    })
  }else{
    next();
  }
});

export default router;
