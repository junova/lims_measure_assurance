import { getToken } from "../../utils/storage";

const USER = {
  state: {
    name: '',
    userName: '',
    token: getToken(),
    roles: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERNAME: (state, userName) => {
      state.userName = userName
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  }
}

export default USER