const getters = {
  name: state => state.user.name,
  userName: state => state.user.userName,
  token: state => state.user.token,
  roles: state => state.user.roles,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
}

export default getters
