import axios from 'axios';
var qs = require('qs');

// 创建 axios 实例
const service = axios.create({
  baseURL: process.env.API_URL,
  timeout: 5000,
  // headers: {
  //   "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
  // },
});

// 拦截请求
service.interceptors.request.use(function (config) {
  const token = localStorage.getItem('token');
  const username = localStorage.getItem('username');
  // 判断请求类型
  if (config.method === 'post') {
    // config.data = qs.stringify(config.data);
    // config.data = qs.stringify({
    //   'userName': username,
    //   'token': token,
    //   ...data,
    // })
  }
  return config;
}, function (error) {
  return Promise.reject(error);
})

export default service;
