// 页面所用到的公共方法

// 表单验证不正确时，改变表单的间距
export function changeFormitemSpace(obj) {
  let $item = obj.querySelectorAll(".el-dialog .el-form-item");
  for(let i = 0; i < $item.length; i++){
      $item[i].style.marginBottom = '15px';
  }
}