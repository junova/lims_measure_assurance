import storage from 'good-storage'

const TokenKey = 'token'
const NameKey = 'name'
const UserNameKey = 'userName'

export function getToken() {
  return storage.get(TokenKey)
}

export function setToken(token) {
  return storage.set(TokenKey, token)
}

export function removeToken() {
  return storage.remove(TokenKey)
}

export function getName() {
  return storage.get(NameKey)
}

export function setName(name) {
  return storage.set(NameKey, name)
}

export function removeName() {
  return storage.remove(NameKey)
}

export function getUserName() {
  return storage.get(UserNameKey)
}

export function setUserName(userName) {
  return storage.set(UserNameKey, userName)
}

export function removeUserName() {
  return storage.remove(UserNameKey)
}
