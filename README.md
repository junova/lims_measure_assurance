# lims_test_assurance

#### 项目介绍
lims检测

#### 软件架构
软件架构说明
vue + element-ui + webpack + axios + vuex


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. 目录结构
  1)service：所有后台请求代码，根据项目模块分文件，相关模块请求定义为英文单词名称（不允许使用拼音代替，使用驼峰式命名法）
  2)assets：字体、图片等静态资源，图片和文字按照项目模块单独分出文件夹进行存放
  3)components：全局公用非业务组件，组件名称使用常用英文（非拼音）形象描述,采用驼峰式命名法，如懒加载组件:lazyLoad
  4)directive：全局指令
  5)filters：全局过滤器，局部过滤器存放于局部文件中
  6)mock：本地测试数据等
  7)router：路由配置统一放在router文件夹下
  8)styles：全局样式
  9)utils：全局公用方法、函数等
  10)view：即视图界面，所有html编写位置，要求按照项目模块分文件夹编写,其中登陆页、首页位于home文件夹下，主界面位于main文件夹下，所有弹框页位于dialog文件夹下，以一级模块划分文件夹
  11)App.vue：入口文件
  12)store：vuex状态存储

